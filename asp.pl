%% -*-sweeprolog-*-
/*
 * run an asp solver and analyse results
 */
:- module(asp, [
                   run/3,
                   op(900,fy,not),
                   op(900,fy,'#minimize'),
                   op(900,fy,'#domain'),
                   op(900,fy,'#hide'),
                   op(900,fy,'#show'),
                   op(700,xfy,'..'),
                   op(700,xfx,'!=')
               ]).

:- autoload(library(lists), [ append/3
                            ]).

:-use_module(library(process)).
:-use_module(library(readutil)).
:-use_module(library(charsio)).

%!  run(+Files, +ClingoOpts, -Results) is semidet
%
%   Run an ASP solver on the specified Files and collect Results. Clingo
%   in the solver. ClingoOpts is a list of clingo options.

run(Files, ClingoOpts, Results) :-
    nb_getval(clasppath, Path),
    append(ClingoOpts, Files, Opts1),
    append(Opts1, ['0'], Opts),
    process_create(Path, Opts, [stdout(pipe(PH)),detached(true)]),
    collect_results(PH, Results),
    close(PH, [force(true)]). % hack

%!  collect_results(++Stream, -Results) is det
%
%   Collect the answer sets produced by clingo.
collect_results(Stream, Results) :-
    read_line_to_codes(Stream, Line),
    chars_codes(LineC, Line),
    (   nb_getval(aspdump, true)
    ->  format("~w~n", [LineC])
    ;   true
    ),
    (   Line == end_of_file
    ->  Results = []
    ;   parse_result_line(LineC, R),
        (   R = end
        ->  Results = []
        ;   (   R = garbage
            ->  collect_results(Stream, Results)
            ;   Results = [R|Results1],
                collect_results(Stream, Results1)
            )
        )
    ).


%!  parse_result_line(+Line, -Result)
%
%   parse a clingo output line.

parse_result_line(Line, R) :-
    phrase(result_line(R), Line, _).


result_line(garbage) -->
    ['c', 'l', 'i', 'n', 'g', 'o'], space, ['v', 'e', 'r', 's', 'i', 'o', 'n'], space,
    versionspec(_).
result_line(garbage) -->
    ['R','e','a','d','i','n','g'], space, ['f','r','o','m'], space,
    dirspec(_).
result_line(garbage) -->
    ['S','o','l','v','i','n','g','.','.','.'].
result_line(answer_num(Num)) -->
    answernum(Num).
result_line(end) -->
    ['S','A','T','I','S','F','I','A','B','L','E'].
result_line(as(AS)) -->
    answer_set(AS).

versionspec([V|VS]) -->
    number(V),
    subversion(VS).

subversion([V|VS]) -->
    ['.'],
    number(V),
    subversion(VS).
subversion([]) -->
    [].

dirspec([C|DS]) -->
    [C],
    {\+ char_type(C, space)},
    dirspec(DS).
dirspec([]) -->
    [].

    % Interpret the "Answer:" lines of clingo output.
answernum(Num) -->
    ['A','n','s','w','e','r',':'], space, number(Num).

    % collect answer sets from clingo output
answer_set([T|L]) -->
    term(LT),
    {
        LT \= [],
        chars_to_term(LT,T)
    },
    end_answer_set(L).

end_answer_set([T|L]) -->
    [' '],
    term(LT),
    {
        LT \= [],
        chars_to_term(LT, T)
    },
    end_answer_set(L).
end_answer_set([]) --> [' '].
end_answer_set([]) --> [].

term([C|L]) -->
    [C],
    {\+ char_type(C,space)},
    term(L).
term([]) -->
    [].


space --> [' '], space.
space --> [].

number(N) -->
    digit(D0),
    digits(D),
    { number_chars(N, [D0|D]) }.

digits([D|T]) -->
    digit(D),
    digits(T).
digits([]) --> [].

digit(D) -->
    [D],
    { char_type(D,digit) }.



/***************************************************************************/
/* Utility predicates                                                      */
/***************************************************************************/

%%      char_codes (?Chars, ?Codes)
%
%       From lists of chars to lists of codes.

chars_codes([],[]).
chars_codes([Ch|L1],[Co|L2]) :-
    char_code(Ch, Co),
    chars_codes(L1,L2).

%!  chars_to_term(+Chars, -Term) is semidet
%
%   Succeed if Term is a term represented by a list of Chars.
chars_to_term(Chars,Term) :-
    append(Chars,['.'],Chars1),
    open_chars_stream(Chars1,S),
    read(S,Term),
    close(S).













