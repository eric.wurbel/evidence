%% -*-sweeprolog-*-
/*
 * Generate :
 * - the ASP representation of a set of formulae
 * - the program computing Wincl(B, µ)
 */
:-module(generator,
         [
             collect_atoms/3,
             gen_atom_rules/2,
             gen_formulae_rules/2,
             gen_formulae_rules/3,
             gen_mu_rules/2,
             generate_form_selector/1,
             init_form_atom/0,
             init_subform_atom/0,
             generate_select_deselect/1,
             gen_show/1,
             gen_heuristic/1
         ]).

:-use_module(library(ordsets)).
:-use_module(library(clpfd)).
:-use_module(library(lists)).
:-use_module(library(apply)).

:- use_module(logic).
:- use_module(asp).

%!  collect_atoms(+LWffs, +IAtoms, -OAtoms) is det
%
%   Succeeds if OAtoms is a set of atoms containing atoms in IAtoms union
%   the atoms found in the list of well formed formulae LWffs.
collect_atoms([], A, A) :- !.
collect_atoms([F|L], InSet, OutSet) :-
    !,
    collect_form_atoms(F, InSet, OSet1),
    collect_atoms(L, OSet1, OutSet).

%!  collect_form_atoms(+Wff, +IAtoms, -OAtoms) is det
%
%%  Succeeds if OAtoms is a set of atoms containing atoms in IAtoms
%%  union the atoms found in the well formed formula Wff.
collect_form_atoms(A & B, InSet, OutSet) :-
    !,
    collect_form_atoms(A, InSet, OSet1),
    collect_form_atoms(B, OSet1, OutSet).
collect_form_atoms(A | B, InSet, OutSet) :-
    !,
    collect_form_atoms(A, InSet, OSet1),
    collect_form_atoms(B, OSet1, OutSet).
collect_form_atoms(A -> B, InSet, OutSet) :-
    !,
    collect_form_atoms(A, InSet, OSet1),
    collect_form_atoms(B, OSet1, OutSet).
collect_form_atoms(A <-> B, InSet, OutSet) :-
    !,
    collect_form_atoms(A, InSet, OSet1),
    collect_form_atoms(B, OSet1, OutSet).
collect_form_atoms(-A,InSet,OutSet) :-
    !,
    collect_form_atoms(A, InSet,OutSet).
collect_form_atoms(A,InSet,OutSet) :-
    prop_atom(A),
    !,
    ord_add_element(InSet,A,OutSet).

%!  gen_atom_rules(?Atoms, ?Rules) is semidet
%
%   Succeeds if Rules is a set of ASP rules representing the generation
%   of interpretations over the list of atoms Atoms.

gen_atom_rules(L, (0 // L // Len)) :-
    length(L, Len).

%!  gen_formulae_rules(?LWffs, ?Rules) is semidet
%
%   Succeeds if Rules is a list of ASP rules representing the formulae
%   in the list of formulae LWffs.

gen_formulae_rules([], true).
gen_formulae_rules([F|Wffs], Rules) :-
    gen_form_tree(F, Tree),
    simplify(Tree, STree),
    next_form_atom(FA),
    gen_formula_rules(STree, FA, R),
    flatten_conjunction(R, RR),
    gen_formulae_rules(Wffs, Rules1),
    conjoin(RR, Rules1, Rules).

%!  gen_formulae_rules(?LWffs, ?Rules, ?Assoc) is semidet
%
%   Succeeds if Rules is a list of ASP rules representing the formulae
%   in the list of formulae LWffs and Assoc is a list which associates
%   the generated formula selector atom with the original formula
%   (useful for printing results).

gen_formulae_rules([], true, []).
gen_formulae_rules([F|Wffs], Rules, [(FA, F)|Assoc]) :-
    gen_form_tree(F, Tree),
    simplify(Tree, STree),
    next_form_atom(FA),
    gen_formula_rules(STree, FA, R),
    flatten_conjunction(R, RR),
    gen_formulae_rules(Wffs, Rules1, Assoc),
    conjoin(RR, Rules1, Rules).

%!  gen_mu_rules(?LWffs, ?Rules) is semidet
%
%   Succeeds if Rules is a list of ASP rules representing the formulae
%   in the list of formulae LWffs. Does not use formula selectors.

gen_mu_rules([], true).
gen_mu_rules([F|Wffs], Rules) :-
    gen_form_tree(F, Tree),
    simplify(Tree, STree),
    gen_formula_rules(STree, nil, R),
    flatten_conjunction(R, RR),
    gen_mu_rules(Wffs, Rules1),
    conjoin(RR, Rules1, Rules).

%!  gen_form_tree(?Wff, ?Tree) is semidet
%
%   Succeed if Tree corresponds to the well formed formula Wff.
%
%   TODO: something wrong with negation. Check.

gen_form_tree(A & B, conj_([TA, TB])) :-
    gen_form_tree(A, TA),
    gen_form_tree(B, TB).
gen_form_tree(A | B, disj_([TA, TB])) :-
    gen_form_tree(A, TA),
    gen_form_tree(B, TB).
gen_form_tree(A -> B, disj_([TA, TB])) :-
    gen_form_tree(-A, TA),
    gen_form_tree(B, TB).
gen_form_tree(A <-> B, conj_([TA, TB])) :-
    gen_form_tree(A -> B, TA),
    gen_form_tree(B -> A, TB).
gen_form_tree(-A, neg_(TA)) :-
    gen_form_tree(A, TA).
gen_form_tree(A, A) :-
    prop_atom(A).

%!  simplify(+Tree, -SimplifiedTree) is det
%
%   SimplifiedTree is the simplification of a formula Tree.

simplify(neg_(neg_(A)), F) :-
    !,
    simplify(A, F).
simplify(disj_(L), F) :-
    select(disj_(L1), L, L1, L2),
    !,
    flatten(L2, L3),
    simplify(disj_(L3), F).
simplify(disj_(L), disj_(LS)) :-
    % \+ select(disj(L1), L, L1, L2)
    !,
    simplify_args(L, LS).
simplify(conj_(L), F) :-
    select(conj_(L1), L, L1, L2),
    !,
    flatten(L2, L3),
    simplify(conj_(L3), F).
simplify(conj_(L), conj_(LS)) :-
    % \+ select(conj(L1), L, L1, L2)
    !,
    simplify_args(L, LS).
    % neither nested neg, disj nor conj
simplify(F, F).

simplify_args([], []).
simplify_args([Arg|L1], [SArg|L2]) :-
    simplify(Arg, SArg),
    simplify_args(L1, L2).


%!  gen_formula_rules(+Tree, +FormulaAtom, -Rules) is semidet
%
%   Rules is a conjunction of ASP rules corresponding to Tree.
%   FormulaAtom is either a formula selector (f_/1) or a subformula
%   flag (sf_/1).

gen_formula_rules(conj_(L), FAtom, Rules) :-
    gen_conjrules(L, FAtom, Rules).
gen_formula_rules(disj_(L), FAtom, Rules) :-
    atomic_forms(L, AF),
    gen_atomic_forms_body(AF, Body1),
    non_atomic_forms(L, NAF),
    length(NAF, L1),
    next_subform_atoms(L1, SAtoms),
    foldl(conjoin, SAtoms, true, Body2),
    (   FAtom = f_(_)
    ->  conjoin(FAtom, Body1, Body3),
        conjoin(Body2, Body3, Body4),
        flatten_conjunction(Body4, Body),
        Rule = ( :- Body )
    ;   conjoin(Body1, Body2, Body),
        (   FAtom = sf_(_)
        ->  Rule = ( FAtom :- Body )
        ;   Rule = ( :- Body ) % no selector at all (Mu case)
        )
    ),
    gen_form_rules_l(NAF, SAtoms, Rules1),
    conjoin(Rule, Rules1, Rules).
gen_formula_rules(neg_(A), FAtom, Rules) :-
    (   FAtom = f_(_)
    ->  Rule = ( :- FAtom, not SubAtom )
    ;
    (   FAtom = sf_(_)
    ->  Rule = ( FAtom :- not SubAtom )
    ;   Rule = ( :- not SubAtom )
    )),
    next_subform_atom(SubAtom),
    gen_formula_rules(A, SubAtom, Sub),
    conjoin(Rule, Sub, Rules).
gen_formula_rules(A, f_(At), ( :- f_(At), not A )) :-
    prop_atom(A).
gen_formula_rules(A, sf_(At), ( sf_(At) :- not A )) :-
    prop_atom(A).
gen_formula_rules(A, FAtom, ( :- not A )) :-
    prop_atom(A),
    FAtom \= f_(_),
    FAtom \= sf_(_).

gen_form_rules_l([], [], true).
gen_form_rules_l([Tree|L1], [FAtom|L2], Rules) :-
    gen_formula_rules(Tree, FAtom, Rules1),
    gen_form_rules_l(L1, L2, Rules2),
    conjoin(Rules1, Rules2, Rules).

%!  gen_conjrules(+L, +FAtom, -Rules) is semidet
%
%   Generate the rules of a conjunction. L is the list of conjuncts.

gen_conjrules([], _, true) :- !.
gen_conjrules([A|L], FAtom, Rules) :-
    prop_atom(A),
    !,
    (   FAtom = f_(_)
    ->  Rule = ( :- FAtom, not A )
    ;
    (   FAtom = sf_(_)
    ->  Rule = (FAtom :- not A)
    ;   Rule = ( :- not A )
    )),
    gen_conjrules(L, FAtom, Rules1),
    conjoin(Rule, Rules1, Rules).
gen_conjrules([neg_(A)|L], FAtom, Rules) :-
    prop_atom(A),
    !,
    (   FAtom = f_(_)
    ->  Rule = ( :- FAtom, A )
    ;
    (   FAtom = sf_(_)
    ->  Rule = ( FAtom :- A)
    ;   Rule = ( :- A )
    )),
    gen_conjrules(L, FAtom, Rules1),
    conjoin(Rule, Rules1, Rules).
gen_conjrules([neg_(A)|L], FAtom, Rules) :-
    % \+ prop_atom(A),
    !,
    next_subform_atom(SAtom),
    (   FAtom = f_(_)
    ->  Rule = ( :- FAtom, SAtom )
    ;
    (   FAtom = sf_(_)
    ->  Rule = ( FAtom :- SAtom )
    ;   Rule = ( :- SAtom )
    )),
    gen_conjrules(L, FAtom, Rules1),
    gen_formula_rules(A, SAtom, Rules2),
    conjoin(Rule, Rules1, Rules3),
    conjoin(Rules2, Rules3, Rules).
    % expand disjunctions directly into the body.
    % Even non flat disjunctions should be incorporated, but, in this case,
    % we have to generate the subformulae atoms (and generate the rules).
gen_conjrules([disj_(D)|L], FAtom, Rules) :-
    !,
    atomic_forms(D, AF),
    gen_atomic_forms_body(AF, Body1),
    non_atomic_forms(D, NAF),
    length(NAF, L1),
    next_subform_atoms(L1, SAtoms),
    foldl(conjoin, SAtoms, true, Body2),
    (   FAtom = f_(_)
    ->  conjoin(FAtom, Body1, Body3),
        conjoin(Body3, Body2, Body4),
        flatten_conjunction(Body4, Body),
        Rule = ( :- Body )
    ;   conjoin(Body1, Body2, Body),
        (   FAtom = sf_(_)
        ->  Rule = ( FAtom :- Body )
        ;   Rule = ( :- Body )
        )
    ),
    gen_form_rules_l(NAF, SAtoms, Rules1),
    conjoin(Rule, Rules1, Rules2),
    gen_conjrules(L, FAtom, Rules3),
    conjoin(Rules2, Rules3, Rules).
gen_conjrules([A|L], FAtom, Rules) :-
    % should not arise.
    % \+ prop_atom(A), \+ A = neg_(_), \+ A = disj_(_)
    !,
    next_subform_atom(SAtom),
    (   FAtom = f_(_)
    ->  Rule = ( :- FAtom, SAtom )
    ;
    (   FAtom = sf_(_)
    ->  Rule = ( FAtom :- SAtom )
    ;   Rule = ( :- SAtom )
    )),
    gen_conjrules(L, FAtom, Rules1),
    gen_formula_rules(A, SAtom, Rules2),
    conjoin(Rule, Rules1, Rules3),
    conjoin(Rules2, Rules3, Rules).

gen_atomic_forms_body([], true).
gen_atomic_forms_body([neg_(A)|L], Body) :-
    !,
    gen_atomic_forms_body(L, Body1),
    conjoin(A, Body1, Body).
gen_atomic_forms_body([A|L], Body) :-
    prop_atom(A),
    !,
    gen_atomic_forms_body(L, Body1),
    conjoin((not A), Body1, Body).


%!  gen_show(-Rules) is det
%
%   Generate #show directives for each opposite selector atom.

gen_show('#show' f_/1).

%!  gen_heuristic(-Rules)
%
%   Generate heuristic rules.

gen_heuristic('#heuristic'(fp_(1..N), [1, false])) :-
    nb_getval(form_atom, N).


/***************************************************************************/

%!  non_atomic_forms(+Forms, ?NAForms)
%
%   Succeed if NAForms unifies with non atomic formulae in Forms.

non_atomic_forms([], []) :- !.
non_atomic_forms([neg_(A)|L1], L2) :-
    prop_atom(A),
    !,
    non_atomic_forms(L1, L2).
non_atomic_forms([A|L1], L2) :-
    prop_atom(A),
    !,
    non_atomic_forms(L1, L2).
non_atomic_forms([F|L1], [F|L2]) :-
    non_atomic_forms(L1, L2).

%!  atomic_forms(+Forms, ?AForms)
%
%   Succeed if AForms unifies with atomic formulae in Forms.
%   an atomic formula is a variable 'a' or the negation of a variable
%   'neg(a)'.

atomic_forms([], []) :- !.
atomic_forms([neg_(A)|L1], [neg_(A)|L2]) :-
    prop_atom(A),
    !,
    atomic_forms(L1, L2).
atomic_forms([A|L1], [A|L2]) :-
    prop_atom(A),
    !,
    atomic_forms(L1, L2).
atomic_forms([_|L1], L2) :-
    atomic_forms(L1, L2).


/***************************************************************************/


:- nb_setval(form_atom, 0).
:- nb_setval(subform_atom, 0).


%%  init_form_atom is det
%%
%%  Initializes the counter for the so-called "formula atoms"
%%  (i.e. selector atoms for formulae).

init_form_atom :-
    nb_setval(form_atom, 0).

%%  next_form_atom(?A) is det
%%
%%  succeed is A unifies with the next formula atom. If unification
%%  fails, the formula atom counter is not incremented.

next_form_atom(f_(N)) :-
    nb_getval(form_atom, Old),
    N #= Old + 1,
    nb_setval(form_atom, N).

%%  init_subform_atom is det
%%
%%  Initializes the counter for the so-called "subformula atoms"
%%  (i.e. selector atoms for subformulae).

init_subform_atom :-
    nb_setval(subform_atom, 0).

%%  next_subform_atom(?A) is det
%%
%%  succeed is A unifies with the next subformula atom. If unification
%%  fails, the subformula atom counter is not incremented.

next_subform_atom(sf_(N)) :-
    nb_getval(subform_atom, Old),
    N #= Old + 1,
    nb_setval(subform_atom, N).

%%  next_subform_atoms(+N, ?L) is det
%%
%%  succeed is L unifies with the N next subformula atoms. 

next_subform_atoms(0, []) :- !.
next_subform_atoms(N, [sf_(ID)|L]) :-
    nb_getval(subform_atom, Old),
    ID #= Old + 1,
    N1 #= N - 1,
    nb_setval(subform_atom, ID),
    next_subform_atoms(N1, L).



%!  generate_form_selector(-Rule)
%
%   generate the formula selector generator.

generate_form_selector(0 // [f_(1..N)] // N) :-
    nb_getval(form_atom, N).

%!  generate_select_deselect(-Rules)
%
%

generate_select_deselect(Rules) :-
    nb_getval(form_atom, N),
    gen_sde(N, Rules).

gen_sde(0, true) :- !.
gen_sde(N, Rules) :-
    R1 = ( fp_(N) :- not f_(N) ),
    N1 #= N - 1,
    gen_sde(N1, Rules1),
    conjoin(R1, Rules1, Rules).










