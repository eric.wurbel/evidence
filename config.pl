:- module(config, [
              load_config/2
          ]).

:- use_module(library(yaml)).

%!  load_config(+Filename, -Config) is det
%
%   Load a configuration file.
load_config(Filename, Config) :-
    expand_file_name(Filename, [ExpFilename|_]),
    absolute_file_name(ExpFilename, AbsFilename),
    yaml_read(AbsFilename, Config),
    !.
load_config(_, nil).

