/*
 * !!! this is a transient test file !!!
 *
 * Compute Bel(B_i) for all B_i\in Wincl(B, µ).
 *
 * The input is an ASP program encoding B and µ.
 * Formulae in B are encoded the usual as follows.
 *
 * a | b translates into
 * :- not a, not b.
 *
 * a & b translates into
 * :- not a
 * :- not b
 *
 * -a translates into not not a, thus a
 *
 * Each formula phi_i generates a new atom c(i) acting as a formula
 * selector. For example, formula phi_1 = (a | (c & -d)) will be encoded
 * :- c(1), not a, not sub(1).
 * sub(1) :- not c.
 * sub(1) :- d.
 *
 * Note the subformula atom.
 *
 * Formula µ is encoded as formulae in B, except that we do not use any
 * selector atom.
 *
 * Aside from the encoded formulae, the program must contain the
 * machinery allowing for the generation of maximal subsets of B which
 * are consistent with µ.
 *
 * Interpretation generator:
 * { a1, a2, ... }.
 * for all atoms ai appearing in B and µ.
 *
 * formulae selectors generator:
 * { c(1..n) }.
 * for all formulae selectors used in the encoding of B.
 *
 * mutual exclusion between formula selector atoms and formula
 * deselectors atoms (primed version of selector atoms.
 *
 * c'(i) :- not c(i).
 * c(i) :- not c'(i).
 *
 * for all selectors.
 *
 * Directive indicating that we are only interrested in c/1 atoms
 * #show c/1.
 *
 * A heuristic which guarantee subset-minimal solutions on c'/1, and
 * thus subset-maximal solutions on c/1.
 * #heuristic c'(1..9). [1,false]
 *
 * This encoding can be splitted acrosse several input files.
 *
 * A future version will generate automatically the program, given B
 * and µ.
 *
 */

:- use_module(library(clpfd)).

:- use_module(asp, [run/3]).
:- use_module(evidence, [global_bba/3, beliefs/3]).

:- set_prolog_flag(prefer_rationals, true).

compute :-
    current_prolog_flag(argv, [N|Files]),
    (   Files = []
    ->  error("no input files")
    ;   true
    ),
    nb_setval(clasppath, path(clingo)),
    run(Files, ['--heuristic=Domain', '--enum-mode=domRec'], Result),
    (   Result = []
    ->  error("no results")
    ;   true
    ),
    get_bis(Result, Bis),
    generate_b(N, B),
    global_bba(B, Bis, GBBA),
    beliefs(Bis, GBBA, Bel),
    print_beliefs(Bel)
    .

test(N, Files) :-
    nb_setval(clasppath, path(clingo)),
    run(Files, ['--heuristic=Domain', '--enum-mode=domRec'], Result),
    (   Result = []
    ->  error("no results")
    ;   true
    ),
    get_bis(Result, Bis),
    generate_b(N, B),
    global_bba(B, Bis, GBBA),
    beliefs(Bis, GBBA, Bel),
    print_beliefs(Bel)
    .


%!  generate_b(N, B, Func)
%
%   generate all formula selectors using the specified functor.

generate_b(N, B, Func) :-
    integer(N),
    var(B),
    generate_b1(1, N, B, Func)
    .

generate_b1(_, 0, [], _) :- !.
generate_b1(N, CountDown, [T|L], Func) :-
    N1 #= N + 1,
    CD1 #= CountDown - 1,
    T =.. [Func, N],
    generate_b1(N1, CD1, L, Func)
    .


generate_b(N, B) :-
    integer(N),
    var(B),
    generate_b1(1, N, B)
    .
generate_b1(_, 0, []) :- !.
generate_b1(N, CountDown, [c(N)|L]) :-
    N1 #= N + 1,
    CD1 #= CountDown - 1,
    generate_b1(N1, CD1, L)
    .

get_bis([], []) :- !.
get_bis([answer_num(_)|L], Bis) :-
    !,
    get_bis(L, Bis)
    .
get_bis([as(Bi)|L], [Bi|Bis]) :-
    !,
    get_bis(L, Bis)
    .

%!  print_beliefs(+Bels) is semidet

print_beliefs([]).
print_beliefs([(Bi, Bel)|Bels]) :-
    format('Bel(~w)=~w~n', [Bi, Bel]),
    print_beliefs(Bels)
    .

%%      error(+Msg)
%
%       Writes an error message ont the user_error stream and throws a
%       plrsf_exception with the same message as argument.

error(Msg) :-
        write(user_error,Msg),
        throw(plrsf_exception(Msg))
        .

%%      error(+Fmt, +Args)
%
%       Formats a message, writes it ont the user_error stream, then
%       throws a plrsf_exception with the same formatted message as
%       argument.

error(Fmt, Args) :-
        format(user_error,Fmt,Args),
        format(atom(A),Fmt,Args),
        throw(plrsf_exception(A))
        .



