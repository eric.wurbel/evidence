%% -*-sweeprolog-*-
/*
 * Fourmulae input/output.
 */

:- module(formula_io,
          [
              load_sof/2,
              write_clauses/2,
              load_raw_collection/3,
              load_raw_prio_collection/4,
              load_prio_sof/3 % +Filename,?Wffs,?Strata
          ]).

:- use_module(library(lists), [ member/2, append/3]).
:- use_module(library(clpfd)).
:- use_module(library(apply)).
:- use_module(library(ordsets)).

% sweeprolog complains it is an unused dependency, but it is used
% indeed (operators defined in asp module).
:- use_module(asp).
:- use_module(utils).
:- use_module(logic, [prop_atom/1]).

%!  load_sof(+Filename, ?Wffs) is semidet
%
%   load the list of well formed formulae Wffs from file Filename.

load_sof(Filename, Wff) :-
    exists_file(Filename),
    !,
    open(Filename, read, Stream),
    read_wff_loop(Stream, Wff).
load_sof(Filename, _) :-
    error('~w: not found~n',[Filename]).

%!  load_prio_sof(+Filename, ?Wffs, ?Strata) is det.
%
%   Load the list of well formed formulae Wffs from Filename. the list
%   is stratified, i.e. the file starts with a term stratum__. The
%   formulae following this term are in the first (prefered) stratum,
%   until we encounter another stratum__ term, which begins the second
%   stratum, and so on until the end of file.
%
%   Strata is a list of lists, each member list is the list of the
%   formula in a stratum. The first stratum is the preferred one.

load_prio_sof(Filename, Wff, Strata) :-
    exists_file(Filename),
    !,
    open(Filename, read, Stream),
    read_wff_loop(Stream, Wff1),
    split_strata(Wff1, Wff, Strata).
load_prio_sof(Filename, _, _) :-
    error('~w: not found~n',[Filename]).


%!  read_wff_loop(++Stream, ?Wff)
%
%   Succeed if reading the Streams provides the list of well formed
%   formulae Wff.

read_wff_loop(Stream, Wff) :-
    read_term(Stream, Wff1, [module(asp)]),
    (   Wff1 == end_of_file
    ->  Wff = []
    ;   read_wff_loop(Stream, Wff2),
        Wff = [Wff1|Wff2]
    ).

split_strata([], [], []).
split_strata([stratum__|Wff1], Wff, [Str1|Strata]) :-
    stratum(Wff1, Wff2, Str),
    list_to_ord_set(Str, Str1),
    split_strata(Wff2, Wff3, Strata),
    append(Str1, Wff3, Wff).

stratum([], [], []).
stratum([stratum__|Wff1], [stratum__|Wff1], []).
stratum([Wff|Wff1], Wff2, [Wff|Stratum]) :-
    Wff \= stratum__,
    stratum(Wff1, Wff2, Stratum).




%!  write_clauses(+Conjunct, +File) is det
%
%   Write a conjunction of terms in a file.

write_clauses(Conjunct, File) :-
    open(File,write,Stream),
    write_conjunct(Conjunct,Stream),
    close(Stream).

write_conjunct((A, B),Stream) :-
    !,
    write_conjunct(A,Stream),
    write_conjunct(B,Stream).
write_conjunct(R,Stream) :-
    write_term(Stream,R,[max_depth(0),numbervars(true),portray(true),module(asp)]),
    (   nb_getval(prog, true)
    ->  write_term(R,[max_depth(0),numbervars(true),portray(true),module(asp)]),
        (   R = '#heuristic'(_, _)
        ->  true
        ;   put('.')
        ),
        nl
    ;   true
    ),

    (   R = '#heuristic'(_, _)
    ->  true
    ;   put(Stream,'.')
    ),
    nl(Stream).

%!  load_raw_collection(+Filename, -B, -Bis) is det
%
%   Load a set B and a collection Bis of subsets of B.

load_raw_collection(FileName, B, Bis) :-
    exists_file(FileName),
    !,
    open(FileName, read, Stream),
    ldraw(Stream, B, Bis).
load_raw_collection(FileName, _, _) :-
    error('~w: not found~n',[FileName]).

ldraw(Stream, B, Bis) :-
    !,
    read_wff_loop(Stream, Terms),
    member(b(Br), Terms),
    is_list(Br),
    maplist(prop_atom, Br),
    list_to_ord_set(Br, B),
    member(collec(Bisr), Terms),
    is_list(Bisr),
    maplist(is_list, Bisr),
    check_bis(B, Bisr, Bis, 1).

check_bis(_, [], [], _).
check_bis(B, [Bir|Bisr], [N/Bi|Bis], N) :-
    list_to_ord_set(Bir, Bi),
    % strict inclusion
    ord_subset(Bi, B),
    \+ ord_seteq(Bi, B),
    N1 #= N + 1,
    check_bis(B, Bisr, Bis, N1).

%!  load_raw_prio_collection(+Filename, -B, -Bis, -Strata) is det
%%
%%  Load a set B, a collection Bis of subsets of B, and a tuple Strata
%%  describing the stratification of B.

load_raw_prio_collection(Filename, B, Bis, Strata) :-
    exists_file(Filename),
    !,
    open(Filename, read, Stream),
    ldraw_prio(Stream, B, Bis, Strata).
load_raw_prio_collection(Filename, _, _, _) :-
    error('~w: not found~n', [Filename]).

ldraw_prio(Stream, B, Bis, Strata) :-
    !,
    read_wff_loop(Stream, Terms),
    % extract and check belied base
    member(b(Br), Terms),
    is_list(Br),
    maplist(prop_atom, Br),
    list_to_ord_set(Br, B),
    % extract and check Bis
    member(collec(Bisr), Terms),
    is_list(Bisr),
    maplist(is_list, Bisr),
    check_bis(B, Bisr, Bis, 1),
    % extract and check stratification
    member(strata(Str), Terms),
    is_list(Str),
    maplist(is_list, Str),
    check_strata(B, Str, Strata, 1).

check_strata(_, [], [], _).
check_strata(B, [Stri|Str], [N/Stratumi|Strata], N) :-
    list_to_ord_set(Stri, Stratumi),
    % strict inclusion
    ord_subset(Stratumi, B),
    \+ ord_seteq(Stratumi, B),
    N1 #= N + 1,
    check_strata(B, Str, Strata, N1).







