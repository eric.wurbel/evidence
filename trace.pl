%%   -*-sweeprolog-*-

/*
    Copyright Éric Würbel, LSIS-CNRS UMR7196, (2012-2023)

    <eric.wurbel@lis.org>

    This software is a computer program whose purpose is to perform
    removed-set fusion of knowledge bases represented by logic programs.

    This software is governed by the CeCILL license under French law and
    abiding by the rules of distribution of free software.  You can use,
    modify and/ or redistribute the software under the terms of the
    CeCILL license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to the source code and rights to
    copy, modify and redistribute granted by the license, users are
    provided only with a limited warranty and the software's author, the
    holder of the economic rights, and the successive licensors have
    only limited liability.

    In this respect, the user's attention is drawn to the risks
    associated with loading, using, modifying and/or developing or
    reproducing the software by the user in light of its specific status
    of free software, that may mean that it is complicated to
    manipulate, and that also therefore means that it is reserved for
    developers and experienced professionals having in-depth computer
    knowledge. Users are therefore encouraged to load and test the
    software's suitability as regards their requirements in conditions
    enabling the security of their systems and/or data to be ensured
    and, more generally, to use and operate it in the same conditions as
    regards security.

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.

    Trace predicates.
*/
:- module(trace, [trace/4]).

:- use_module(library(clpfd)).


%!  trace(+Fmt, +Args, +Options, +TraceLevel) is det.
%
%   Print a trace message if the actual trace level present in the
%   Options is greater or equal to the requested TraceLevel.  Fmt is
%   a formating atom for the format/2 predicate, and Args is a list of
%   argument for the formating atom.

trace(Fmt, Args, Options, TraceLevel) :-
    memberchk(trace(N), Options),
    N #>= TraceLevel,
    !,
    atom(Fmt),
    is_list(Args),
    format(Fmt, Args).
trace(_, _, _, _).












