%%   -*-sweeprolog-*-

:- begin_tests(prio).

:- use_module(evidence).
:- use_module(library(ordsets), [list_to_ord_set/2]).

b(1, [f1, f2, f3, f4, f5, f6, f7]).
bi(1, [1/[f1,f2], 2/[f3,f2], 3/[f4,f2], 4/[f5, f6, f7]]).
strata(1, [1/[f1, f3], 2/[f2], 3/[f4, f5, f6, f7]]).

order_bases([], []).
order_bases([N/Bi|Bis], [N/OBi|OBis]) :-
    list_to_ord_set(Bi, OBi),
    order_bases(Bis, OBis).

get_data(Num, B, Bis, Strata) :-
    set_prolog_flag(prefer_rationals, true),
    b(Num, B),
    bi(Num, Bis1),
    strata(Num, Strata),
    order_bases(Bis1, Bis).

test(simple_non_normalized01, [setup(get_data(1, B, Bis, Strata))]) :-
    global_prio_bba(B, Bis, Strata, GBBA, [trace(0),nonorm(true)]),
    beliefs_prio(Bis, Strata, GBBA, Beliefs),
    trace_global_bba(GBBA, [trace(2)], 1),
    print_beliefs(Beliefs).
    

:- end_tests(prio).
