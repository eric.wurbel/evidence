%% -*-sweeprolog-*-
/************************************************************************
 * Computing W_bel(B, µ)
 *
 * Main program.
 ************************************************************************
 */
:- use_module(library(clpfd)).
:- use_module(library(lists), [ member/2
                              ]).
:- use_module(library(ordsets)).
:- use_module(library(optparse)).

:- use_module(utils).
:- use_module(formula_io).
:- use_module(asp).
:- use_module(generator).
:- use_module(logic).
:- use_module(evidence).
:- use_module(config).

:- set_prolog_flag(prefer_rationals, true).

optspec([
    [opt(asp), type(boolean), default(false), shortflags([a]), longflags([asp]),
     help(['print asp run output (no effect with --raw)'])],
    [opt(inclusion), type(boolean), default(false), shortflags([i]),
     longflags(['inclusion-max']),
     help(['print maximal subsets wrt set inclusion.',
           'This option is not compatible with --raw'])],
    [opt(nonorm), type(boolean), default(false), shortflags([n]), longflags(['no-norm']),
     help(['avoid normalization step.'])],
    [opt(prog), type(boolean), default(false), shortflags([p]), longflags([prog]),
     help(['print generated program (no effect with --raw)'])],
    [opt(prioritized), type(boolean), default(false), shortflags([z]), longflags([prioritized]),
     help(['prioritized beliefs measurement over a stratified',
           'knowledge base.'])],
    [opt(raw), type(boolean), default(false), shortflags([r]), longflags([raw]),
     help(['raw mode : the only input file contains the',
           'description of a collection of subsets of',
           'some set. The whole set is described by a',
           'b/1 fact, the argument being the list of',
           'elements. The collection of subsets is',
           'described by a collec/1 fact, the argument',
           'being a list of subsets, each subset being',
           'in turn a list of elements.'])],
    [opt(lambda), type(boolean), default(false), shortflags([l]), longflags([lambda]),
     help(['use lambda masses instead of basic belief masses.'])],
    [opt(trace), type(integer), default(0), shortflags([t]), longflags([trace]),
     help(['trace the computation.'])],
    [opt(sort), type(boolean), default(false), shortflags([s]), longflags([sort]),
     help(['sort results by belief value.'])]
]).

%!  go
%
%   Main predicate.
go :-
    set_prolog_flag(prefer_rationals, true),
    current_prolog_flag(version, Ver),
    (   Ver =< 80000
    ->  current_prolog_flag(argv, [_|Args])
    ;   current_prolog_flag(argv, Args)
    ),
   go(Args).

go(Args) :-
    % command line parsing
    optspec(Spec),
    catch(opt_parse(Spec, Args, Opts, PosArgs), Err, errora_syn('~w~n', [Err], Spec)),
    memberchk(asp(V), Opts),
    nb_setval(aspdump, V),
    memberchk(prog(Prog), Opts),
    nb_setval(prog, Prog),
    (   memberchk(raw(true), Opts)
    ->  (   memberchk(inclusion(true), Opts)
        ->  errora("incompatible options -r and -i")
        ;   (   memberchk(prioritized(true), Opts)
            ->  raw_prio_evidence_computation(Opts, PosArgs)
            ;   raw_evidence_computation(Opts, PosArgs)
            )
        )
    ;   (   memberchk(prioritized(true), Opts)
        ->  full_prio_evidence_computation(Opts, PosArgs)
        ;   full_evidence_computation(Opts, PosArgs)
        )
    ).

%!  full_evidence_computation(+Opts, +PosArgs)
%
%   Computes and prints the credibility of each subset of B which is
%   maximal-consistent with mu.

full_evidence_computation(Opts, PosArgs) :-
    [BFile, MuFile] = PosArgs,
    load_config('~/.wbel.cfg', Config),
    set_config(Config),
    nb_setval(clasppath, '/home/wurbel/local/anaconda3/envs/potassco/bin/clingo'),
    init_form_atom,
    init_subform_atom,
    % temporary file for ASP prog generation.
    tmp_file(wbel, TmpFile),
    % Loading B and Mu
    load_sof(BFile, B),
    load_sof(MuFile, Mu),
    generate(B, Mu, TmpFile, Assoc),
    run([TmpFile], ['--heuristic=Domain', '--enum-mode=domRec'], ASPResults),
    collect_results(ASPResults, WinclResults, 1),
    (   memberchk(inclusion(true), Opts)
    ->  print_wincl(WinclResults, Assoc)
    ;   true
    ),
    nb_getval(form_atom, N),
    generate_b(N, BSelects, f_),
    (   memberchk(lambda(true), Opts)
    ->  global_lambda(BSelects, WinclResults, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        lambda_beliefs(WinclResults, GBBA, Beliefs)
    ;   global_bba(BSelects, WinclResults, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        beliefs(WinclResults, GBBA, Beliefs)
    ),
    (   memberchk(sort(true), Opts)
    ->  sort(3, @>=, Beliefs, FinalBeliefs)
    ;   FinalBeliefs = Beliefs
    ),
    print_beliefs(FinalBeliefs, Assoc).
full_evidence_computation(_, PosArgs) :-
    [_, _] \= PosArgs,
    errora_syn("Bad number of positional args").

%!  full_prio_evidence_computation(+Opts, +PosArgs) is det.
%
%   Computes and prints the prioritized credibility of each subset of
%   B which is maximal-consistent with mu.

full_prio_evidence_computation(Opts, PosArgs) :-
    [BFile, MuFile] = PosArgs,
    nb_setval(clasppath, '/home/wurbel/local/anaconda3/envs/potassco/bin/clingo'),
    init_form_atom,
    init_subform_atom,
    % temporary file for ASP prog generation.
    tmp_file(wbel, TmpFile),
    % Loading B and Mu
    load_prio_sof(BFile, B, Strata),
    load_sof(MuFile, Mu),
    generate(B, Mu, TmpFile, Assoc),
    run([TmpFile], ['--heuristic=Domain', '--enum-mode=domRec'], ASPResults),
    collect_results(ASPResults, WinclResults, 1),
    (   memberchk(inclusion(true), Opts)
    ->  print_wincl(WinclResults, Assoc)
    ;   true
    ),
    nb_getval(form_atom, N),
    generate_b(N, BSelects, f_),
    strata_selectors(Strata, StrSels, Assoc),
    (   memberchk(lambda(true), Opts)
    ->  unimplemented, % TODO
        global_prio_lambda(BSelects, WinclResults, StrSels, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        lambda_prio_beliefs(WinclResults, StrSels, GBBA, Beliefs)
    ;   global_prio_bba(BSelects, WinclResults, StrSels, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        beliefs_prio(WinclResults, StrSels, GBBA, Beliefs)
    ),
    (   memberchk(sort(true), Opts)
    ->  sort(3, @>=, Beliefs, FinalBeliefs)
    ;   FinalBeliefs = Beliefs
    ),
    print_beliefs(FinalBeliefs, Assoc).
full_prio_evidence_computation(_, PosArgs) :-
    [_, _] \= PosArgs,
    errora_syn("Bad number of positional args").


%!  raw_evidence_computation(+Opts, +Args)
%
%   Computes and prints the credibility of a collection of subsets.

raw_evidence_computation(Opts, PosArgs) :-
    % hack
    set_prolog_flag(prefer_rationals, true),
    [SetFile] = PosArgs,
    load_raw_collection(SetFile, B, Bis),
    (   memberchk(lambda(true), Opts)
    ->  global_lambda(B, Bis, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        lambda_beliefs(Bis, GBBA, Beliefs)
    ;   global_bba(B, Bis, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        beliefs(Bis, GBBA, Beliefs)
    ),
    (   memberchk(sort(true), Opts)
    ->  sort(3, @>=, Beliefs, FinalBeliefs)
    ;   FinalBeliefs = Beliefs
    ),
    print_raw_beliefs(FinalBeliefs).
raw_evidence_computation(_, PosArgs) :-
    [_] \= PosArgs,
    errora_syn("Bad number of positional args").

%!  raw_evidence_computation(+Opts, +Args)
%
%   Computes and prints the prioritized credibility of a collection of
%   subsets of a stratified set.

raw_prio_evidence_computation(Opts, PosArgs) :-
    % hack
    set_prolog_flag(prefer_rationals, true),
    [SetFile] = PosArgs,
    load_raw_prio_collection(SetFile, B, Bis, Strata),
    (   memberchk(lambda(true), Opts)
    ->  unimplemented, % TODO implement this
        global_prio_lambda(B, Bis, Strata, GBBA, Opts),
        trace_global_bba(GBBA, Opts, 1),
        lambda_prio_beliefs(Bis, Strata, GBBA, Beliefs)
    ;   global_prio_bba(B, Bis, Strata, GBBA, Opts),
        trace_global_bba(GBBA, Opts,1),
        beliefs_prio(Bis, Strata, GBBA, Beliefs)
    ),
    (   memberchk(sort(true), Opts)
    ->  sort(3, @>=, Beliefs, FinalBeliefs)
    ;   FinalBeliefs = Beliefs
    ),
    print_raw_prio_beliefs(FinalBeliefs).
raw_prio_evidence_computation(_, PosArgs) :-
    [_] \= PosArgs,
    errora_syn("Bad number of positional args").

%!  set_config(+Config)
set_config(Config) :-
    is_dict(Config),
    Config.get(clasppath, '/usr/bin/clingo') = ClaspPath,
    nb_setval(clasppath, ClaspPath),
    !.



%!  print_raw_beliefs(+Bels) is det
%
%   Prints the belief function value of a collection of sets.
%
%   Bels is a list of tuples (Bi, Bel) where Bi is a ordered set and
%   Bel the associated belief value.

print_raw_beliefs([]).
print_raw_beliefs([bel(I, Bi, Bel)|Bels]) :-
    format('Bel(~w/~w)=~w~n', [I, Bi, Bel]),
    print_raw_beliefs(Bels).


%!  print_raw_prio_beliefs(+Bels) is det.
%
%   Prints the prioritized belief function value of a collection of
%   subsets of a stratified set.
%
%   Bels is a list of tuples (I, Bi, Bel) where I in the index of the
%   set (one based), Bi is the set and Bel is a list containing the
%   belief value for each stratum.

print_raw_prio_beliefs([]).
print_raw_prio_beliefs([bel(I, Bi, Bel)|L]) :-
    format('Bel(~w/~w)=~w~n', [I, Bi, Bel]),
    print_raw_prio_beliefs(L).

%!  print_beliefs(+Bels, +Assoc) is semidet
%
%   Prints the beliefs of sets of formulae.
%
%   Bels is a list of triples (Li, Bi, Bel) where Li is the index of
%   the base, Bi is a set of formulae selectors, ans assoc is a list
%   associationg formulae selectors with formulae.

print_beliefs([], _).
print_beliefs([bel(Li, Bi, Bel)|Bels], Assoc) :-
    expand_formulae(Bi, Assoc, Forms),
    format('Bel(~w={~w})=~w~n', [Li, Forms, Bel]),
    print_beliefs(Bels, Assoc).


%!  print_wincl(+Bels,+Assoc) is semidet
%
%   print sets of formulas.

print_wincl([], _).
print_wincl([N/Bi|Bels], Assoc) :-
    expand_formulae(Bi, Assoc, FBi),
    format('B~w=~w~n', [N, FBi]),
    print_wincl(Bels, Assoc).

%!  expand_formulae(?SetOfSelectors, ?Assoc, ?SetOfForms)
%
%   Succeed if SetOfSelectors is a set of formulae selectors
%   corresponding to the set of formulae SetOfForms according to the
%   association Assoc.
expand_formulae([], _, []).
expand_formulae([Select|Sels], Assoc, [Form|Forms]) :-
   member((Select, Form), Assoc),
   expand_formulae(Sels, Assoc, Forms).


%!  generate_b(N, B, Func)
%
%   generate all formula selectors using the specified functor.

generate_b(N, B, Func) :-
    integer(N),
    var(B),
    generate_b1(1, N, B, Func).

generate_b1(_, 0, [], _) :- !.
generate_b1(N, CountDown, [T|L], Func) :-
    N1 #= N + 1,
    CD1 #= CountDown - 1,
    T =.. [Func, N],
    generate_b1(N1, CD1, L, Func).



%!  strata_selectors(+Strata, -StrataSels, +Assoc) is det.
%
%   Succeeds if StrataSels is a list od lists of formula selectors
%   corresponding to formulae in the list of list Strata .

strata_selectors(Strata, Selectors, Assoc) :-
    strata_selectors2(1, Strata, Selectors, Assoc).

strata_selectors2(_, [], [], _).
strata_selectors2(N, [Str|Strata], [N/StrSels|Sels], Assoc) :-
    stratum_sels(Str, StrSels, Assoc),
    N1 #= N + 1,
    strata_selectors2(N1, Strata, Sels, Assoc).

stratum_sels([], [], _).
stratum_sels([Form|FL], [Sel|SL], Assoc) :-
    member((Sel, Form), Assoc),
    stratum_sels(FL, SL, Assoc).






%!  collect_results(+ASPRes, -WinclRes)
%
%   collect ASP answer sets
%   Each answer set represent subbase of B which is maximaly consistent
%   with mu. Number each base (starting from 1), that is, the results is
%   a list of i/List terms.

collect_results([], [], _) :- !.
collect_results([as(L)|L1], [N/SL|L2], N) :-
    !,
    list_to_ord_set(L, SL),
    N1 #= N + 1,
    collect_results(L1, L2, N1).
collect_results([_|L1], L2, N) :-
    collect_results(L1, L2, N).

%!  generate(+B, +Mu, +TmpFile, -Assoc) is det
%
%   Generate the asp program reprensenting the computation of Wincl(B,
%   µ). Assoc is a list establishing the correspondence between
%   formulae selector atoms and formulae themselve.

generate(B, Mu, File, Assoc) :-
    collect_atoms(B, [], Atoms1),
    collect_atoms(Mu, Atoms1, Atoms),
    gen_formulae_rules(B, Rules1, Assoc),
    gen_mu_rules(Mu, Rules2),
    gen_atom_rules(Atoms, Rules3),
    generate_form_selector(Rules4),
    generate_select_deselect(Rules5),
    gen_show(Rules6),
    gen_heuristic(Rules7),
    conjoin(Rules1, Rules2, RR1),
    conjoin(RR1, Rules3, RR2),
    conjoin(RR2, Rules4, RR3),
    conjoin(RR3, Rules5, RR4),
    conjoin(RR4, Rules6, RR5),
    conjoin(RR5, Rules7, RR6),
    flatten_conjunction(RR6, Rules),
    write_clauses(Rules, File).


% --------------------------------------------------------------------------
% handle external representation. This should not be in a module.
% --------------------------------------------------------------------------

:- dynamic portray/1.
:- multifile portray/1.

portray(Min // BList // Max) :-
    write_term(Min, [max_depth(0), numbervars(true)]),
    write('{'),
    plist2(BList),
    write('}'),
    write_term(Max, [max_depth(0), numbervars(true)]).

plist2([]).
plist2([E]) :-
    !,
    write(E).
plist2([E|L]) :-
    write(E),
    write(';'),
    plist2(L).


portray('#heuristic'(A, H)) :-
    write('#heuristic '),
    write_term(A, [max_depth(0), numbervars(true)]),
    write('. '),
    write_term(H, [max_depth(0), numbervars(true)]).






