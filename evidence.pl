%% -*-sweeprolog-*-
/* a quickly hacked computation of dempster combination rule

*/
:- module(evidence, [
                        global_bba/4, beliefs/3, trace_global_bba/3,
                        global_lambda/4, lambda_beliefs/3,
                        global_prio_bba/5, % +B,+Bis,+Strata,-GBBA,+Options
                        beliefs_prio/4,
                        print_beliefs/1
                    ]).

:- use_module(library(ordsets), [ ord_intersect/3,
                                  ord_subset/2,
                                  ord_union/3,
                                  ord_intersection/3
                                ]).

:- use_module(library(lists), [ append/3,
                                last/2,
                                select/3
                              ]).
:- use_module(library(clpfd)).

:- use_module(trace).
:- use_module(library(assoc), [empty_assoc/1, put_assoc/4, get_assoc/3, 
                               assoc_to_list/2]).

:- set_prolog_flag(prefer_rationals, true).



%!  basic_bbas(+B, +Bis, -BBAs, +Opts) is det
%
%   Compute the basic belief assignments of subsets of B in collection
%   Bis.
%
%   The resulting BBAs is a list of triplets (BaseLabel, Set, Mass)
%   where BaseLabel is a label denoting the intersected subbases
%   used to build the Set (subbases are labeled with an integer starting
%   from 1, the full base is labeled with 0).
%
%   Uses rational arithmetic.
%
%   We use the is/2 predicate instead of the more practical #=/2 from
%   the clp(fd) module because it does not work whith rationals. We plan
%   to investigate the use of clp(q).

basic_bbas(B, Bis, BBAs, Opts) :-
    length(B, CardB),
    basic_bba2(B, CardB, Bis, BBAs, Opts).

%!  basic_bba2(+B, +CardB, +Bis, -BBAs, Opts) is det
%
%   Utility predicate for basic_bbas/3.

basic_bba2(_, _, [], [], _).
basic_bba2(B, CardB, [N/Bi|Bis], [[([N], Bi, BBABi), ([0], B, BBAB)]|BBAs], Opts) :-
    length(Bi, CardBi),
    BBABi is CardBi / CardB,
    BBAB is 1 - BBABi,
    trace_bba(N/Bi, N/Bi, BBABi, Opts, 1),
    trace_bba(N/Bi, 0/B, BBAB, Opts, 1),
    basic_bba2(B, CardB, Bis, BBAs, Opts).

%!  trace_bba(+RefSet, +Set, +Value, +Options, +TraceLevel) is det

trace_bba(RefSet, Set, Value, Options, TraceLevel) :-
    trace('mu_{~w}(~w) = ~w~n', [RefSet, Set, Value], Options, TraceLevel).
trace_bba(_, _, _, _, _).

%!  basic_prio_bbas(+B, +Bis, +Strata, -BBAs, +Opts) is det.
%
%   Computes the basic belief assignments of subsets of stratified
%   belief base B in collection Bis.
%
%   Bis is a list of N/L terms, where N is the index (1 based) of the
%   subbase, and L a list of formulae.  Strata describes the
%   stratification of B.  It is a list of N/L terms, where N is the
%   index (1 based) of a stratum, and L a list of formulae.
%
%   BBas is the list of computed basic belief assignments. Each belief
%   assignment is a triple (Label, Set, Mass) where Label describes
%   the intersected sets. Set is a list of formulae representing the
%   intersection, and Mass is the associated belief mass. Label is
%   a list of terms b(N) and s(N), where b(N) represents the subbase
%   with index N, with index 0 corresponding the whole belief base
%   B. s(N) represents stratum with index N.
%   
%   Uses rational arithmetic.
%
%   We use the is/2 predicate instead of the more practical #=/2 from
%   the clp(fd) module because it does not work whith rationals. We plan
%   to investigate the use of clp(q).

basic_prio_bbas(B, Bis, Strata, BBAs, Opts) :-
    length(B, CardB),
    basic_prio_bba2(B, CardB, Bis, Strata, BBAs, Opts).

basic_prio_bba2(_, _, [], _, [], _).
basic_prio_bba2(B, CardB, [N/Bi|Bis], Strata, [Mi|BBAs], Opts) :-
    basic_prio_bi(B, CardB, N/Bi, Strata, Mi, 0, Opts),
    basic_prio_bba2(B, CardB, Bis, Strata, BBAs, Opts).

basic_prio_bi(B, _, N/Bi, [], [([b(0)], B, Mass)], RunningSum, Opts) :-
    Mass is 1 - RunningSum,
    trace_bba_prio(N/Bi, [b(0)]/B, Mass, Opts, 1).
basic_prio_bi(B, CardB, N/Bi, [Si/Stratum|Strata],
              [([b(N),s(Si)], Set, Mass)|OutMi], RunningSum, Opts) :-
    ord_intersection(Bi, Stratum, Set),
    length(Set, Card),
    Mass is Card/CardB,
    RS1 is RunningSum + Mass,
    trace_bba_prio(N/Bi, [b(N), n(Si)]/Set, Mass, Opts, 1),
    basic_prio_bi(B, CardB, N/Bi, Strata, OutMi, RS1, Opts).

%!  trace_bba_prio(+RefSet, +Set, +Value, +Options, +TraceLevel) is det

trace_bba_prio(RefSet, Set, Value, Options, TraceLevel) :-
    trace('mu_{~w}(~w) = ~w~n', [RefSet, Set, Value], Options, TraceLevel).
trace_bba_prio(_, _, _, _, _).


%!  combine(+M1, +M2, -M12, +Options) is det
%
%   combines two belief assigments (Dempster rule).
%   each member of the lists is a tuple (Set, Mass).

combine([], _, [], _).
combine([Mass1|Masses1], Masses2, RMasses, Opts) :-
    combine1(Mass1, Masses2, RMasses1, Opts),
    combine(Masses1, Masses2, RMasses2, Opts),
    append(RMasses1, RMasses2, RMasses).

%!  combine1(+MassTriplet, +Masses, -ResultMasses, Opts)
%
%   Combine a belif assignment with a list of belief assignments.

combine1(_, [], [], _).
combine1((Label1, Set1, Mass1), [(_, Set2, _)|L], Result, Opts) :-
    % empty intersection case
    ord_intersect(Set1, Set2, []), % empty intersection case
    trace('~w inter ~w = emptyset~n', [Set1, Set2], Opts, 2),
    combine1((Label1, Set1, Mass1), L, Result, Opts).
combine1((Label1, Set1, Mass1),
         [(Label2, Set2, Mass2)|L], [(RLabel, RSet, RMass)|Result], Opts) :-
    ord_intersect(Set1, Set2, RSet),
    RSet \= [], % nonempty intersection case
    RMass is Mass1 * Mass2,
    (   memberchk(0, Label1)
    ->  RLabel = Label2
    ;   (   memberchk(0, Label2)
        ->  RLabel = Label1
        ;   ord_union(Label1, Label2, RLabel)
        )
    ),
    trace('~w inter ~w = ~w : ~w~n', [Set1, Set2, RSet, RMass], Opts, 2),
    combine1((Label1, Set1, Mass1), L, Result, Opts).

%!  combine_prio(+M1, +M2, -M12, +Options) is det
%
%   combines two belief assigments (Dempster rule).
%   each member of the lists is a tuple (Set, Mass).

combine_prio([], _, [], _).
combine_prio([Mass1|Masses1], Masses2, RMasses, Opts) :-
    combine_prio1(Mass1, Masses2, RMasses1, Opts),
    combine_prio(Masses1, Masses2, RMasses2, Opts),
    append(RMasses1, RMasses2, RMasses).

%!  combine_prio1(+MassTriplet, +Masses, -ResultMasses, Opts)
%
%   Combine a belif assignment with a list of belief assignments.

combine_prio1(_, [], [], _).
combine_prio1((Label1, Set1, Mass1), [(_, Set2, _)|L], Result, Opts) :-
    % empty intersection case
    ord_intersect(Set1, Set2, []), % empty intersection case
    trace('~w inter ~w = emptyset~n', [Set1, Set2], Opts, 2),
    combine_prio1((Label1, Set1, Mass1), L, Result, Opts).
combine_prio1((Label1, Set1, Mass1),
         [(Label2, Set2, Mass2)|L], [(RLabel, RSet, RMass)|Result], Opts) :-
    ord_intersect(Set1, Set2, RSet),
    RSet \= [], % nonempty intersection case
    RMass is Mass1 * Mass2,
    (   memberchk(b(0), Label1)
    ->  RLabel = Label2
    ;   (   memberchk(b(0), Label2)
        ->  RLabel = Label1
        ;   ord_union(Label1, Label2, RLabel)
        )
    ),
    trace('~w inter ~w = ~w : ~w~n', [Set1, Set2, RSet, RMass], Opts, 2),
    combine_prio1((Label1, Set1, Mass1), L, Result, Opts).


%!  normalize(+BBA, -NormBBA) is det
%
%   Succeeds if NormBBA is the normalized version of BBA.
%
%   In practice, normalisation takes place if the sum of the masses is
%   not equal to 1.

normalize(M1, M1) :-
    sum_mass(M1, 1).
normalize(M1, M2) :-
    sum_mass(M1, S1),
    S1 \== 1,
    Factor is 1/S1,
    mult(Factor, M1, M2).

%!  merge_unique(+BBA1, -BBA2) is det
%
%   Merge duplicate sets and add their masses.

merge_unique([], []).
merge_unique([(Label, Set, M1)|B1], [(Label, Set, M2)|B3]) :-
    collect_masses_unique((Label, Set), B1, Masses, B2),
    M2 is M1 + Masses,
    merge_unique(B2, B3).

%!  collect_masses_unique(+LabelSet, +BBas, -Masses, -B2) is det
%
%   Masses is the sum of all masses of be belief assignments in BBAs
%   concerning (Label, Set). B2 containts the remaining
%   assignments.

collect_masses_unique(_, [], 0, []).
collect_masses_unique((Label, Set), [(Label, Set, M1)|BBAs], Masses, FinalBBAs) :-
    collect_masses_unique((Label, Set), BBAs, M2, FinalBBAs),
    Masses is M1 + M2.
collect_masses_unique((Label, Set), [(Label1, S1, M1)|BBAs], Masses,
                      [(Label1, S1, M1)|FinalBBAs]) :-
    (   Set \== S1 ; Label \== Label1
    ),
    collect_masses_unique((Label, Set), BBAs, Masses, FinalBBAs).

%!  sum_mass(+IMasses, -Sum) is semidet
%
%   Sum is the sum of the masses in IMasses BBA

sum_mass([], 0).
sum_mass([(_, _, M)|L], S) :-
    sum_mass(L, S1),
    S is S1 + M.

%!  mult(+F, +L1, -L2) is semidet
%
%   Multiply a BBA L1 by a factor F.

mult(_, [], []).
mult(F, [(Lb1, S1, N1)|L1], [(Lb1, S1, N2)|L2]) :-
    N2 is N1 * F,
    mult(F, L1, L2).



%!  global_bba(+B, +Bis, -GBBA, +Opts)
%
%   Computes the global BBA of a belief base B.
%   
%   B is the the belief base. Bis is the collection subsets of B which
%   are maximal-consistent with mu (wrt set inclusion). These are
%   terms I/List where I is the subbase index and List is the list of
%   formulae in the subbase. GBBA is the computed global belief
%   assignment. It is a list of triples (IdxList, Forms, Value) where
%   IdxList is a list of subbase indexes (special index 0 denotes the
%   whole belief base), Forms is the list of formulae resulting from
%   the intersection of the preceding subbases, and Value is the
%   associated computed belief mass.

global_bba(B, Bis, GBBA, Opts) :-
    trace('---- basic BBAs --------------------------------------~n', [], Opts, 1),
    basic_bbas(B, Bis, [B1|BasicBBAs], Opts),
    % B1: first triplet
    global_bba2(B1, BasicBBAs, GBBA, Opts).

%!  global_bba2(+BBa, +BBas, GBBA, Opts)
%
%   BBa is a list of belief assignments.  BBas is a list of lists of
%   belief assignments.  Each belief assignment is a triplet
%   (BaseLabel, Set, Mass) where BaseLabel is a list of indexes of the
%   intersected subbases producing the Set (subbases are labeled with
%   an integer starting from 1, the full base is labeled with 0).

global_bba2(B, [], B, _).
global_bba2(B, [BBa|BBas], GBBA, Opts) :-
    trace('---- combining ~w & ~w -----------------------------~n', [B, BBa], Opts, 2),
    combine(B, BBa, B1, Opts),
    merge_unique(B1, B2),
    (   memberchk(nonorm(true), Opts)
    ->  B2 = B3
    ;   normalize(B2, B3)
    ),
    global_bba2(B3, BBas, GBBA, Opts).



%!  global_prio_bba(+B, +Bis, +Strata, -GBBA, +Options) is det.
%
%   Computes the global BBA associated with stratified belief base
%   B.
%
%   Bis is the collection of subsets if B which are maximally
%   consistent with mu (wrt set inclusion). These are terms I/List,
%   where I is the subbase index and List the list of formulae
%   (special index 0 denotes the whole belief base). Strata describes
%   thes stratification of belief base B. it is a list of terms
%   N/List, where N is the stratum index (starting at 1, 1 being the
%   prefered stratum) and List is the list of formulae in the stratum.
%
%   GBBA is the computed global assignment. it is a list of triples
%   (Label, Set, Mass), where label is the label representing the
%   intersected sets. It is a list of terms bi(N) or s(N). A bi(N)
%   term represents the subbase of B with index N, and a term s(N)
%   represents a stratum with index N. In both bases N is a positive
%   integer.

global_prio_bba(B, Bis, Strata, GBBA, Opts) :-
    trace('---- basic prio BBAs --------------------------------~n', [], Opts, 1),
    basic_prio_bbas(B, Bis, Strata, [B1|BasicBBAs], Opts),
    % B1: first triplet
    global_prio_bba2(B1, BasicBBAs, GBBA, Opts).

global_prio_bba2(B, [], B, _).
global_prio_bba2(B, [BBa|BBas], GBBA, Opts) :-
    trace('---- combining ~w & ~w -----------------------------~n', [B, BBa], Opts, 2),
    combine_prio(B, BBa, B1, Opts),
    merge_unique(B1, B2),
    (   memberchk(nonorm(true), Opts)
    ->  B2 = B3
    ;   normalize(B2, B3)
    ),
    global_prio_bba2(B3, BBas, GBBA, Opts).




%!  beliefs(+Bis, +GBBA, -Bel)
%
%   Bel is the set of beliefs for each Bi, given the global BBA GBBA.

beliefs([], _, []).
beliefs([Ni/Bi|Bis], GBBA, [bel(Ni, Bi, Bel)|Bels]) :-
    belief(Ni/Bi, GBBA, Bel),
    beliefs(Bis, GBBA, Bels).

belief(_, [], 0).
belief(Ni/Bi, [(Nj, Bj, M)|GBBA], Bel) :-
    ord_subset(Bj, Bi),
    memberchk(Ni, Nj),
    belief(Ni/Bi, GBBA, Bel1),
    Bel is M + Bel1.
belief(Ni/Bi, [(Nj, Bj, _)|GBBA], Bel) :-
    (   \+ ord_subset(Bj, Bi) ; \+ memberchk(Ni, Nj) ),
    belief(Ni/Bi, GBBA, Bel).


%!  print_beliefs(+Bels, +Assoc) is semidet
%
%   essentially used for debugging.

print_beliefs([]).
print_beliefs([bel(Li, Bi, Bel)|Bels]) :-
    format('Bel(~w={~w})=~w~n', [Li, Bi, Bel]),
    print_beliefs(Bels).

%!  beliefs_prio(+Bis, +Strata, +GBBA, -Beliefs) is det.
%
%   Computes the beliefs of each Bis. The belief of a Bi subbase is
%   a term bel(I, Bi, Bel) where bel is a list which first item
%   is the belief of Bi&S1 (S1 being the first stratum), the second
%   item is the belief of Bi&S2, etc.

beliefs_prio(Bis, Strata, GBBA, Bels) :-
    empty_assoc(BelAssoc),
    build_prio_bel_assoc(BelAssoc, Bis, Strata, BelAssoc1),
    beliefs_prio1(GBBA, BelAssoc1, BelAssoc2),
    assoc_to_prio_bel(BelAssoc2, Bels).

%!  build_prio_bel_assoc(+InAssoc, Bis, Strata, OutAssoc) is det.
%
%   Build an association list associating to each label [b(i),s(k)]
%   a tuple (Bi, 0) for each Bi in Bis and each stratum in Strata.

build_prio_bel_assoc(A, [], _, A).
build_prio_bel_assoc(InAssoc, [Ni/Bi|Bis], Strata, OutAssoc) :-
    build_prio_bel_assoc2(InAssoc, Ni/Bi, Strata, A1),
    build_prio_bel_assoc(A1, Bis, Strata, OutAssoc).


build_prio_bel_assoc2(A, _, [], A).
build_prio_bel_assoc2(InA, Ni/Bi, [Si/_|Strata], OutA) :-
    put_assoc([b(Ni),s(Si)], InA, (Bi, 0), A1),
    build_prio_bel_assoc2(A1, Ni/Bi, Strata, OutA).


beliefs_prio1([], A, A).
beliefs_prio1([(Label, _, Mass)|GBBA], InA, OutA) :-
    last(Label, s(S)), % last elem of label is a strata
    select(s(S), Label, Label1), % Label1 is Label without stratum
    update_bel(S, Label1, Mass, InA, A1),
    beliefs_prio1(GBBA, A1, OutA).
beliefs_prio1([_|GBBA], InA, OutA) :-
    beliefs_prio1(GBBA, InA, OutA).

update_bel(_, [], _, A, A).
update_bel(S, [b(I)|Label], Mass, InA, OutA) :-
    get_assoc([b(I), s(S)], InA, (Bi, BValue)),
    NewB is BValue + Mass,
    put_assoc([b(I), s(S)], InA, (Bi, NewB), A1),
    update_bel(S, Label, Mass, A1, OutA).

assoc_to_prio_bel(Assoc, Bels) :-
    assoc_to_list(Assoc, BList),
    collect_bels(BList, Bels).


collect_bels([], []).
collect_bels([[b(I),s(1)]-(Bi,Bel)|L], [bel(I, Bi, [Bel|BiBels])|Bels]) :-
    collect_bi_bels(I, 2, L, L1, BiBels),
    collect_bels(L1, Bels).

collect_bi_bels(_, _, [], [], []).
collect_bi_bels(I, S, [[b(I), s(S)]-(_, Bel)|L], L1, [Bel|Bels]) :-
    % the succession condition is perhaps unnecessary.
    S1 #= S + 1,
    collect_bi_bels(I, S1, L, L1, Bels).
collect_bi_bels(I1, _, [[b(I2), S]-V|L], [[b(I2), S]-V|L], []) :-
    I1 \= I2.


%!  trace_global_bba(+GBBA, +Opts, +Level) is det
%
%   trace the global BBAs.

trace_global_bba(GBBA, Opts, Level) :-
    memberchk(trace(N), Opts),
    N #>= Level,
    !,
    trace_global_bba2(GBBA).
trace_global_bba(_, _, _).

trace_global_bba2([]) :- !.
trace_global_bba2([(Labels, Set, Mass)|GBBA]) :-
    write('m_mu('),
    print_set_label(Labels),
    format(' = ~w ) = ~w~n', [Set, Mass]),
    trace_global_bba2(GBBA).

print_set_label([]) :- !.
print_set_label([S]) :- !, write(S).
print_set_label([S|L]) :-
    !,
    char_code(X, 0x2229), % intersection
    format('~w~w', [S, X]),
    print_set_label(L).

/*********************************************************************/
/* using lambda masses instead of belief masses                      */
/*********************************************************************/

%!  basic_lambda(+B, +Bis, -BBAs, +Opts) is det
%
%   Compute the basic lambda assignments of subsets of B in
%   collection Bis.
%
%   The resulting BBAs is a list of triplets (BaseLabel, Set, Mass)
%   where BaseLabel is a label denoting the Set (subbases are labeled
%   with an integer starting from 1, the full base is labeled with 0).
%
%   This label is present in the Bis list. Each element of this list is
%   a Label/Set term where Set is a set.
%

basic_lambda(B, Bis, BBAs, Opts) :-
    length(B, CardB),
    basic_lambda2(B, CardB, Bis, BBAs, Opts).

%!  basic_lambda2(+B, +CardB, +Bis, -BBAs) is det
%
%   Utility predicate for basic_bbas/3.

basic_lambda2(_, _, [], [], _).
basic_lambda2(B, CardB, [N/Bi|Bis], [[([N], Bi, BBABi), ([0], B, BBAB)]|BBAs], Opts) :-
    length(Bi, CardBi),
    BBABi #= CardBi,
    BBAB #= CardB - CardBi,
    trace_bba(N/Bi, N/Bi, BBABi, Opts, 1), % trace_bba should be ok.
    trace_bba(N/Bi, 0/B, BBAB, Opts, 1),
    basic_lambda2(B, CardB, Bis, BBAs, Opts).

%!  combine_lambdas(+M1, +M2, -M12, +Options) is det
%
%   combines two lambda belief assigments (the rule is inspired
%   by the Dempster rule but uses integer arithmetic. It performs no
%   normalization). each member of the lists is a tuple (Set, Mass).

combine_lambda([], _, [], _).
combine_lambda([Mass1|Masses1], Masses2, RMasses, Opts) :-
    combine_lambda1(Mass1, Masses2, RMasses1, Opts),
    combine_lambda(Masses1, Masses2, RMasses2, Opts),
    append(RMasses1, RMasses2, RMasses).

combine_lambda1(_, [], [], _).
combine_lambda1((Label1, Set1, Mass1), [(_, Set2, _)|L], Result, Opts) :-
    % empty intersection case
    ord_intersect(Set1, Set2, []), % empty intersection case
    trace('~w inter ~w = emptyset~n', [Set1, Set2], Opts, 2),
    combine_lambda1((Label1, Set1, Mass1), L, Result, Opts).
combine_lambda1((Label1, Set1, Mass1), [(Label2, Set2, Mass2)|L], [(RLabel, RSet, RMass)|Result], Opts) :-
    ord_intersect(Set1, Set2, RSet),
    RSet \= [], % nonempty intersection case
    RMass #= Mass1 * Mass2,
    (   memberchk(0, Label1)
    ->  RLabel = Label2
    ;   (   memberchk(0, Label2)
        ->  RLabel = Label1
        ;   ord_union(Label1, Label2, RLabel)
        )
    ),
    trace('~w inter ~w = ~w : ~w~n', [Set1, Set2, RSet, RMass], Opts, 2),
    combine_lambda1((Label1, Set1, Mass1), L, Result, Opts).

%!  merge_unique(+BBA1, -BBA2) is det
%
%   Merge duplicate sets and add their masses.

merge_unique_lambda([], []).
merge_unique_lambda([(Label, Set, M1)|B1], [(Label, Set, M2)|B3]) :-
    collect_lambda_masses_unique((Label, Set), B1, Masses, B2),
    M2 #= M1 + Masses,
    merge_unique(B2, B3).

%!  collect_lambda_masses_unique(+LabelSet, +BBas, -Masses, -B2) is det
%
%   Masses is the sum of all masses of be belief assignments in BBAs
%   concerning (Label, Set). B2 containts the remaining
%   assignments.

collect_lambda_masses_unique(_, [], 0, []).
collect_lambda_masses_unique((Label, Set), [(Label, Set, M1)|BBAs], Masses, FinalBBAs) :-
    collect_masses_unique((Label, Set), BBAs, M2, FinalBBAs),
    Masses #= M1 + M2.
collect_lambda_masses_unique((Label, Set), [(Label1, S1, M1)|BBAs], Masses, [(Label1, S1, M1)|FinalBBAs]) :-
    (   Set \== S1 ; Label \== Label1
    ),
    collect_lambda_masses_unique((Label, Set), BBAs, Masses, FinalBBAs).

%!  global_lambda(+B, +Bis, -GBBA, +Nonorm)
%
%   B is the the belief base. Bis is the collection subsets of B which
%   are maximal-consistent with mu (wrt set inclusion). GBBA is the
%   computed global belied assignment.
%

global_lambda(B, Bis, GBBA, Opts) :-
    trace('---- basic BBAs --------------------------------------~n', [], Opts, 1),
    basic_lambda(B, Bis, [B1|BasicBBAs], Opts),
    global_lambda2(B1, BasicBBAs, GBBA, Opts).

%!  global_bba2(+BBa, +BBas, GBBA, Opts)
%
%   Each BBa is a triplet (BaseLabel, Set, Mass)
%   where Base Label is a label denoting the Set (subbases are labeled
%   with an integer starting from 1, the full base is labeled with 0).

global_lambda2(B, [], B, _).
global_lambda2(B, [BBa|BBas], GBBA, Opts) :-
    trace('---- combining ~w & ~w -----------------------------~n', [B, BBa], Opts, 2),
    combine_lambda(B, BBa, B1, Opts),
    merge_unique_lambda(B1, B2),
    global_lambda2(B2, BBas, GBBA, Opts).


%!  lambda_beliefs(+Bis, +GBBA, -Bel)
%
%   Bel is the set of beliefs for each Bi, given the global lambda BBA
%   GBBA.

lambda_beliefs([], _, []).
lambda_beliefs([Ni/Bi|Bis], GBBA, [bel(Ni, Bi, Bel)|Bels]) :-
    lambda_belief(Ni/Bi, GBBA, Bel),
    lambda_beliefs(Bis, GBBA, Bels).

lambda_belief(_, [], 0).
lambda_belief(Ni/Bi, [(Nj, Bj, M)|GBBA], Bel) :-
    ord_subset(Bj, Bi),
    memberchk(Ni, Nj),
    lambda_belief(Ni/Bi, GBBA, Bel1),
    Bel #= M + Bel1.
lambda_belief(Ni/Bi, [(Nj, Bj, _)|GBBA], Bel) :-
    (   \+ ord_subset(Bj, Bi) ; \+ memberchk(Ni, Nj) ),
    lambda_belief(Ni/Bi, GBBA, Bel).

/**********************************************************************/
/* test data                                                          */
/**********************************************************************/

% b([c(1), c(2), c(3), c(4), c(5), c(6), c(7), c(8), c(9)]).
% bi([
%        [c(1), c(4), c(5)],
%        [c(1), c(3), c(4)],
%        [c(1), c(2), c(3)],
%        [c(1), c(2), c(5)],
%        [c(2), c(4), c(5), c(6), c(7), c(8), c(9)],
%        [c(2), c(3), c(4), c(6), c(7), c(8), c(9)]
%    ]).

% test01(GBBA, Bels) :-
%     b(B),
%     bi(Bis),
%     global_bba(B, Bis, GBBA,[nonorm(false),trace(0)]),
%     beliefs(Bis, GBBA, Bels),
%     print_beliefs(Bels).






















